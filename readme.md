# How to use

* `git clone https://gitlab.com/fernandesd/bookstore.git`
* `cd bookstore`
* `cp .envexample .env`
* Configurar o banco no .env
* `composer install`
* `php artisan migrate`
* `php artisan serve`

* [http://localhost:8000](localhost:8000)

