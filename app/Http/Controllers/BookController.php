<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use Orchestra\Parser\Xml\Facade as XmlParser;
use SimpleXMLElement;


class BookController extends Controller
{
 
    public function index()
    {
        $books = Book::all();
        return $books;
    }


    public function create()
    {
        $url = 'http://homologacao.motor-reserva.com.br/sis_motor_reserva/api/bookstore/index.php?bookstore=get';
        
        $xml = XmlParser::load($url);
        $books = $xml->getContent();
        
        foreach($books->book as $key => $book){          
            
            $b = new Book();
            $b->title = $book->title;            
            $b->category = $book['category'];
            $b->year = $book->year;
            $b->price = $book->price;
           
             
            foreach($book->author as $key => $author){
                $b->author .= $author.', ';  
            }
            
            $b->author = substr($b->author, 0, -1);
            $b->author = substr($b->author, 0, -1);                        
                    
            $b->save();  
        }

        return redirect()->route('book.index');        
       
    }    
 
   
    public function show(Book $book)
    {
        return $book->toJson();
    }

   
   
}
