<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'category', 'title_lang', 'title', 'author', 'year', 'price'
    ];
}
